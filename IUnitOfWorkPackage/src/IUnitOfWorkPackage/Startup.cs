﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IUnitOfWorkPackage.Startup))]
namespace IUnitOfWorkPackage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
